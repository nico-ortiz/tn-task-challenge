/* eslint-disable no-undef */

'use strict'

const mongoose = require('mongoose')
const _ = require('lodash')
const httpStatus = require('http-status')
const nock = require('nock')
const {
  boot,
  cleanDB,
  api,
  expect,
  request,
  chance,
  models,
} = require('../../index')

const { Task } = models

const PATH = '/api/tasks/:taskId'
      
describe(`GET ${PATH}`, () => {
  let task
  before(async () => {
    await boot()
    if (!nock.isActive()) nock.activate()
  })

  beforeEach(async () => {
    await cleanDB()
    task = await Task.create({
      title: chance.sentence(),
    })
  })

  after(async () => {
    await cleanDB()
    nock.restore()
  })
  
  it(`Shoud return ${httpStatus.OK} with task`, async () => {
    const path = _.replace(PATH, ':taskId', task._id.toString())
    const { status, body } = await request(api)
      .get(path)
    expect(status).to.equal(httpStatus.OK)
    expect(body).to.be.an('Object')
      .to.has.property('title').eq(task.title)
  })

  it(`Shoud return ${httpStatus.NOT_FOUND} with invalid id`, async () => {
    const path = _.replace(PATH, ':taskId', mongoose.Types.ObjectId().toString())
    const { status } = await request(api)
      .get(path)
    expect(status).to.equal(httpStatus.NOT_FOUND)
  })
})
