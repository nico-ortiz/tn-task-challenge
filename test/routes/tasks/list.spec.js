/* eslint-disable no-undef */

'use strict'

const _ = require('lodash')
const httpStatus = require('http-status')
const nock = require('nock')
const {
  boot,
  cleanDB,
  api,
  expect,
  request,
  chance,
  models,
} = require('../../index')

const { Task } = models

const PATH = '/api/tasks'

nock('https://hipsum.co/api/')
  .persist()
  .get('/')
  .query({ type: 'hipster-centric', sentences: 1 })
  .reply(200, [chance.sentence()])
      
describe(`GET ${PATH}`, () => {
  before(async () => {
    await boot()
    if (!nock.isActive()) nock.activate()
  })

  beforeEach(async () => {
    await cleanDB()
  })

  after(async () => {
    await cleanDB()
    nock.restore()
  })
  
  it(`Shoud return ${httpStatus.OK} with default number of task 3`, async () => {
    const { status, body } = await request(api)
      .get(PATH)
    expect(status).to.equal(httpStatus.OK)
    expect(body).to.be.an('array')
      .to.have.lengthOf(3)
    _.forEach(body, (task) => {
      expect(task).to.has.property('title')
      expect(task).to.has.property('id')
    })
  
    const tasksDB = await Task.find({ _id: { $in: _.map(body, 'id') } }).lean()
    expect(tasksDB).to.be.an('array')
      .to.have.lengthOf(3)
  })

  it(`Should return ${httpStatus.OK} with N number of task`, async () => {
    const count = chance.integer({ min: 1, max: 100 })
    const { status, body } = await request(api)
      .get(`${PATH}?count=${count}`)
    expect(status).to.equal(httpStatus.OK)
    expect(body).to.be.an('array')
      .to.have.lengthOf(count)

    const tasksDB = await Task.find({ _id: { $in: _.map(body, 'id') } }).lean()
    expect(tasksDB).to.be.an('array')
      .to.have.lengthOf(count)
  })

  it(`Should return ${httpStatus.BAD_REQUEST} when send invalid query param`, async () => {
    const param = chance.word()
    const value = chance.word()
    const { status } = await request(api)
      .get(`${PATH}?${param}=${value}`)
    expect(status).to.equal(httpStatus.BAD_REQUEST)
  })
})
