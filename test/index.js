'use strict'

const mongoose = require('mongoose')
const Promise = require('bluebird')
const chai = require('chai')
const chaiHttp = require('chai-http')
const chaiAsPromised = require('chai-as-promised')
const Chance = require('chance')
const httpStatus = require('http-status')
const nock = require('nock')

const api = require('../src/api')

const db = require('../src/db')

const Task = require('../src/models/Task.model')

chai.use(chaiHttp)
chai.use(chaiAsPromised)

const boot = async () => {
  await db.connectMongo()
}

/**
 * Removes all documents from every MongoDB collection.
 *
 * @returns {Promise} A promise that is fulfilled if all documents from
 * all collections could be removed.
 * */
const cleanDB = async () => Promise.map(
  mongoose.modelNames(),
  // eslint-disable-next-line comma-dangle
  async (modelName) => mongoose.model(modelName).deleteMany({})
)

module.exports = {
  boot,
  cleanDB,
  api,
  expect: chai.expect,
  request: chai.request,
  httpStatus,
  chance: new Chance(),
  models: {
    Task,
  },
}
