'use strict'

const { apiPort } = require('./configs')
const db = require('./db')
const app = require('./api')

const boot = async () => {
  // Connect to MongoDB.
  await db.connectMongo()
  console.info('Connected to MongoDB')
  console.log('apiPort :>> ', apiPort)
  // Start the server.
  app.listen(apiPort, () => console.info(`API Running on port ${apiPort}.`))
}

// Boot the application.
boot()
  .catch((err) => console.error('Error while booting the application', err))
