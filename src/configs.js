'use strict'

const _ = require('lodash')

const {
  NODE_ENV,
  API_PORT,
  MONGO_DB_URI,
  MONGO_DB_URI_TEST,
} = process.env

module.exports = {
  env: _.defaultTo(NODE_ENV, 'development'),
  apiPort: _.defaultTo(API_PORT, 3000),
  mongoDBUri: NODE_ENV === 'test'
    ? _.defaultTo(MONGO_DB_URI_TEST, 'mongodb://mongodb:27017/test-nt-challenge')
    : _.defaultTo(MONGO_DB_URI, 'mongodb://mongodb:27017/nt-challenge'),
}
