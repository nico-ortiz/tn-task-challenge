'use strict'

const { objectId } = require('../../schemas')

const update = {
  params: {
    taskId: objectId().required(),
  },
}

module.exports = update
