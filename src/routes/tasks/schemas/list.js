'use strict'

const { Joi } = require('celebrate')

const find = {
  query: {
    count: Joi.number().default(3).min(1),
  },
}

module.exports = find
