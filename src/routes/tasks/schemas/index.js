'use strict'

const listSchema = require('./list')
const taskAccess = require('./taskAccess')

module.exports = {
  listSchema,
  taskAccess,
}
