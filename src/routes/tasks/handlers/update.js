'use strict'

const httpStatus = require('http-status')
const { STATUS } = require('../../../models/Task.constants')

const update = async (req, res, next) => {
  try {
    const {
      task,
    } = req.locals

    console.log(`Task ${task._id} DONE`)
    task.status = STATUS.DONE

    await task.save()
    res.sendStatus(httpStatus.NO_CONTENT)
  } catch (error) {
    next(error)
  }
}

module.exports = update
