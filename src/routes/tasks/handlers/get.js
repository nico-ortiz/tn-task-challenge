'use strict'

const httpStatus = require('http-status')

const get = async (req, res, next) => {
  try {
    const {
      task,
    } = req.locals

    res.status(httpStatus.OK).json(task)
  } catch (error) {
    next(error)
  }
}

module.exports = get
