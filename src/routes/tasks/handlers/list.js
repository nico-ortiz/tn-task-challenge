'use strict'

const Promise = require('bluebird')
const Hipsum = require('../../../lib/hipsum')
const { HIPSUM_TYPE } = require('../../../lib/hipsum/constants')
const Task = require('../../../models/Task.model')

const list = async (req, res, next) => {
  try {
    const {
      count,
    } = req.query

    const tasks = await Promise.map(new Array(count), async () => {
      const { data } = await Hipsum.getSentences(HIPSUM_TYPE.HIPSTER_CENTRIC)
      const [title] = data
      const newTask = await Task.create({ title })
      return newTask
    })

    res.json(tasks)
  } catch (error) {
    console.log('error :>> ', error)
    next(error)
  }
}

module.exports = list
