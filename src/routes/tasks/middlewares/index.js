'use strict'

const fetchTask = require('./fetchTask')

module.exports = {
  fetchTask,
}
