'use strict'

const httpErrors = require('http-errors')

const Task = require('../../../models/Task.model')

const fetchTask = async (req, res, next) => {
  const { taskId } = req.params
  try {
    const task = await Task.findById(taskId)
    if (!task) throw new httpErrors.NotFound('Task not found')
    req.locals.task = task
    return next()
  } catch (error) {
    return next(error)
  }
}

module.exports = fetchTask
