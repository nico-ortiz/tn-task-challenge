'use strict'

const { Router } = require('express')
const { celebrate } = require('celebrate')

const {
  list,
  update,
  get,
} = require('./handlers')

const {
  fetchTask,
} = require('./middlewares')

const {
  listSchema,
  taskAccess,
} = require('./schemas')

const router = new Router()

router.get('/', celebrate(listSchema), list)
router.get('/:taskId', celebrate(taskAccess), fetchTask, get)
router.put('/:taskId', celebrate(taskAccess), fetchTask, update)

module.exports = router
