'use strict'

const express = require('express')
const { errors } = require('celebrate')
const morgan = require('morgan')
const helmet = require('helmet')
const cors = require('cors')
const httpError = require('http-error')
const { httpErrorHandler } = require('../error-handlers')

const taskRouter = require('./tasks')

const router = express.Router()

router.use((req, res, next) => {
  req.locals = {}
  res.locals = {}
  next()
})

router.use(helmet())
router.use(cors())
router.use(express.json())
router.use(morgan(':date[web] - :method :url :status - :user-agent'))

router.use('/api/tasks', taskRouter)
router.use((req, res, next) => next(new httpError.NotFound('Resource not found')))

router.use(errors())
router.use(httpErrorHandler)

module.exports = router
