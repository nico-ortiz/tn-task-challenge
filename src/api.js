'use strict'

const express = require('express')
const router = require('./routes')

const app = express()
// Mount routes.
app.use('/', router)

module.exports = app
