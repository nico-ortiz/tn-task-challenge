'use strict'

const mongoose = require('mongoose')
const _ = require('lodash')

const { Schema } = mongoose

const { STATUS } = require('./Task.constants')

const enumStatus = Object.values(STATUS)
const TaskSchema = new Schema({
  title: {
    type: String,
    trim: true,
    require: [true, 'title is required'],
  },
  status: {
    type: String,
    trim: true,
    default: STATUS.PENDING,
    enum: {
      values: enumStatus,
      msg: `invalid status {VALUE}, must be one of [${enumStatus}]`,
    },
  },
}, {
  collection: 'tasks',
  timestamps: true,
  toJSON: {
    versionKey: false,
    virtuals: true,
    transform(doc, ret) {
      const docData = _.omit(ret, ['_id'])
      docData.id = ret._id
      return docData
    },
  },
})

module.exports = mongoose.model('Task', TaskSchema)
