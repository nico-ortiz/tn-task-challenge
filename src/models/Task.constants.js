'use strict'

module.exports = {
  STATUS: {
    PENDING: 'pending',
    DONE: 'done',
  },
}
