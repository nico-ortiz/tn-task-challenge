'use strict'

module.exports = {
  HIPSUM_TYPE: {
    HIPSTER_CENTRIC: 'hipster-centric',
    HIPSTER_LATIN: 'hipster-latin',
  },
}
