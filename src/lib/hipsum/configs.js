'use strict'

const _ = require('lodash')

const {
  HIPSUM_API_URL,
} = process.env

module.exports = {
  hipsumApiUrl: _.defaultTo(HIPSUM_API_URL, 'https://hipsum.co/api/'),
}
