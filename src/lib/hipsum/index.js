'use strict'

const axios = require('axios')
const { hipsumApiUrl } = require('./configs')

const api = axios.create({
  baseURL: hipsumApiUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
})

module.exports = {
  getSentences: (type, numberSentences = 1) => api.get(`/?type=${type}&sentences=${numberSentences}`),
}
