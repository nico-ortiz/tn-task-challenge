'use strict'

const mongoose = require('mongoose')
const { mongoDBUri } = require('./configs')

mongoose.Promise = require('bluebird')

const connect = async (opts = {}) => {
  const defaultOpts = {
    autoIndex: false,
    useNewUrlParser: true,
    useCreateIndex: true,
  }

  const options = Object.assign(defaultOpts, opts)
  console.log('Connecting to MongoDB with options: %O', options)

  await mongoose.connect(mongoDBUri, options)
  console.log('Connected to MongoDB successfully')

  return mongoose.connection
}

module.exports = {
  connectMongo: connect,
}
