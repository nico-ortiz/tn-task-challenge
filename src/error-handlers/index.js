'use strict'

const httpErrorHandler = require('./httpErrorHandler')

module.exports = {
  httpErrorHandler,
}
