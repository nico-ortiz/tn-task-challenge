# TRUENORTH Challenge #

Create a task api that has 2 endpoints:
GET /api/tasks that generate N tasks (N is dynamic, default is 3)

* Tasks are composed of a UUID and a Title.

* Titles are fetched from the Hipsum API (https://hipsum.co/the-api/)

* This particular API is not idempotent, so all the invocations to /tasks will return a different list.


PUT /api/tasks that mark one task as done.

* This endpoint will only create a log entry

#### Prerequisites
* [Git](https://git-scm.com/downloads)
* [Node JS](https://nodejs.org/en/)
* [Docker](https://www.docker.com)
* [Docker Compose](https://docs.docker.com/compose/install/)

#### 1. Clone the repo
```bash
git clone https://Nikasio@bitbucket.org/nico-ortiz/tn-task-challenge.git 
cd tn-task-challenge
```

#### 2. Startup API
```bash
npm run start:docker
```
#### 2. Run UNIT TEST
```bash
npm run test:docker
```
