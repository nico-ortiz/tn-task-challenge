FROM node:14.15.4-alpine

WORKDIR /srv/api

COPY package*.json ./

RUN npm install
RUN npm install -g nodemon

COPY . .

EXPOSE 3000
